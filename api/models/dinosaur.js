
const mongoose = require('mongoose');

const dinosaurSchema = mongoose.Schema({
  _id: mongoose.Schema.Types.ObjectId,
  name: {
    type: String,
    required: true,
  },
  era: {
    type: String,
    required: true,
  },
  diet: {
    type: String,
    required: true,
  },
  createdAt: {
    type: Number,
  },
  updatedAt: {
    type: Number,
  },
  userId: {
    type: Number,
  },

});

module.exports = mongoose.model('Dinosaur', dinosaurSchema);
