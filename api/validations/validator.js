const Joi = require('joi');

module.exports = (schema, options) => (req, res, next) => {
  const result = Joi.validate(req.body, schema);
  if (result.error) {
    return res.status(500).json({
      error: result.error.details[0].message,
    });
  }
  next();
};
