const Joi = require('joi');

const dietArray = ['herbivore', 'omnivore', 'carnivore'];
const eraArray = ['triassic', 'jurassic', 'cretaceous'];
const dinosaurCreateValidatior = Joi.object().keys({
  name: Joi.string().required(),
  diet: Joi.string().required().valid(dietArray),
  era: Joi.string().required().valid(eraArray),
});
const dinosaurUpdateValidatior = Joi.object().keys({
  name: Joi.string().required(),
  diet: Joi.string().required().valid(dietArray),
  era: Joi.string().required().valid(eraArray),
  _id: Joi.string().required(),
});

module.exports = { dinosaurCreateValidatior, dinosaurUpdateValidatior };
