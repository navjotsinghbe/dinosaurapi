const express = require('express');
const user = require('../controllers/user');
const dinosaur = require('../controllers/dinosaur');

const router = express.Router();
router.use('/user', user);
router.use('/dinosaur', dinosaur);
module.exports = router;
