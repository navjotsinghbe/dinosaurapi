const mongoose = require('mongoose');

const env = process.env.NODE_ENV || 'development';
const config = require('../config')[env];

const dbURI = `mongodb+srv://${config.DB_USER}:${config.DB_PASSWORD}${config.DB_HOST}${config.DB_NAME}?retryWrites=true`;

mongoose.connect(dbURI, { useNewUrlParser: true }).then(() => {
  console.log('DB connection successfull');
}).catch((e) => {
  console.log(e);
});

mongoose.Promise = global.Promise;


module.exports = mongoose;
