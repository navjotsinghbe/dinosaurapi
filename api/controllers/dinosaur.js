const express = require('express');

const router = express.Router();
const mongoose = require('mongoose');
const auth = require('../middlewares/auth');
//mongoose model
const Dinosaur = require('../models/dinosaur');

// validations schemas are to validate the request parameters
const {
  dinosaurCreateValidatior,
  dinosaurUpdateValidatior,
} = require('../validations/dinosaurValidationSchema');

// validator is a middleware to validate the api request parameters
const validator = require('../validations/validator');

/**
 * Creates a new dinosaur
 * @param {string} name - dinosaur name
 * @param {string} era - dinosaur era
 * @param {string} diet - dinosaur diet
 * @returns {object} dinosaur db object
 */
router.post('/', validator(dinosaurCreateValidatior), auth, async (req, res) => {
  // in auth middleware decoded token gives userId
  const userData = { ...req.userData };
  // unix utc time
  const time = Math.floor(new Date() / 1000);
  try {
    // preparing model object to save in db
    const dinosour = new Dinosaur({
      _id: new mongoose.Types.ObjectId(),
      name: req.body.name,
      era: req.body.era,
      diet: req.body.diet,
      userId: userData.userId,
      createdAt: time,
      updatedAt: time,
    });
    // saving to db
    const saving = await dinosour.save();

    res.status(201).json({
      success: true,
      message: 'saved',
      data: saving,
    });
  } catch (error) {
    res.status(500).json({
      success: false,
      message: 'Internal server error',
      error,
    });
  }
});
/**
 * Creates a new dinosaur
 * @returns [{object}] dinosaur list
 */
router.get('/', auth, async (req, res) => {
  const userData = { ...req.userData };
  try {
    // finding dinosaur only user has created
    const dinosaurList = await Dinosaur.find({ userId: userData.userId });
    res.status(200).json({
      success: true,
      dinosaurList,
    });
  } catch (error) {
    res.status(500).json({
      success: false,
      message: 'Internal server error',
      error,
    });
  }
});
/**
 * Creates a new dinosaur
 * @param {string} era - dinosaur era
 * @param {string} diet - dinosaur diet
 * @returns[{object}] dinosaur list
 */
router.get('/search', auth, async (req, res) => {
  const userData = { ...req.userData };
  const era = req.query.era;
  const diet = req.query.diet;

  // building a search query to search on the basis of request parameters
  const searchQuery = { userId: userData.userId };
  if (era) {
    searchQuery.era = era;
  }
  if (diet) {
    searchQuery.diet = diet;
  }
  try {
    const dinosaurList = await Dinosaur.find(searchQuery);
    res.status(200).json({
      success: true,
      dinosaurList,
    });
  } catch (error) {
    res.status(500).json({
      success: false,
      message: 'Internal server error',
      error,
    });
  }
});

/**
 * Creates a new dinosaur
 * @param {string} name - dinosaur name
 * @param {string} era - dinosaur era
 * @param {string} diet - dinosaur diet
 * @param {string} _id - dinosaur id
 * @returns {object} dinosaur updated object
 */

router.patch('/', validator(dinosaurUpdateValidatior), auth, async (req, res) => {
  const userData = { ...req.userData };
  const dinosaurId = req.body._id;
  const time = Math.floor(new Date() / 1000);

  try {
    // checking the ownership of the user for the dinosaur
    const owner = await Dinosaur.findOne({
      userId: userData.userId,
      _id: dinosaurId,
    });
    if (!owner || !owner._id) {
      return res.status(401).json({
        success: false,
        message: 'you are not authorise to update this dinosaur',
      });
    }
    const updateQuery = {
      _id: dinosaurId,
      name: req.body.name,
      era: req.body.era,
      diet: req.body.diet,
      updatedAt: time,
    };
    // updating the dinosaur to db
    // {new:true} return updated ojbect
    const updatedDinosaur = await Dinosaur.findOneAndUpdate(dinosaurId, updateQuery, { new: true });
    res.status(201).json({
      success: true,
      updatedDinosaur,
    });
  } catch (error) {
    res.status(500).json({
      success: false,
      message: 'Could not update the dinosaur',
      error,
    });
  }
});
/**
 * Creates a new dinosaur
 * @param {string} _id - dinosaur id
 * @returns {object} success message of deletetion
 */
router.delete('/', auth, async (req, res) => {
  const userData = { ...req.userData };
  const dinosaurId = req.body._id;
  try {
    const owner = await Dinosaur.findOne({
      userId: userData.userId,
      _id: dinosaurId,
    });
    if (!owner || !owner._id) {
      return res.status(401).json({
        success: false,
        message: 'you can not delete this dinosaur',
      });
    }
    const deletedDinosaur = await Dinosaur.findByIdAndDelete(dinosaurId);
    res.status(201).json({
      success: true,
      message: 'deleted successfully',
    });
  } catch (error) {
    res.status(500).json({
      success: false,
      message: 'Could not delete the dinosaur',
      error,
    });
  }
});

module.exports = router;
