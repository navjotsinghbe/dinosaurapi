const express = require('express');

const router = express.Router();
const jwt = require('jsonwebtoken');
// to get the token it is a dummy login without db
router.get('/login', (req, res) => {
  try {
    const token = jwt.sign({
      email: 'navjotsinghbe@gmail.com',
      userId: 1,
    }, process.env.JWT_KEY, {});
    res.status(200).json({
      success: true,
      message: 'login successful',
      token,
    });
  } catch (error) {
    res.status(500).json({
      success: false,
      message: 'login unsuccessful',
      error,
    });
  }
});

module.exports = router;
