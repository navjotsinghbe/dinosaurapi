# Dinosaur API

Everything you need to know about the APIs

Repositry link : https://bitbucket.org/navjotsinghbe/dinosaurapi/src/master/

## Steps

* Run `npm install`
* Run `npm start`
* Run `npm test` to run tests

### config file is not on bitbucket as it has passwords to my mongo atlas,you can put yours or if you need i can send you as well


### First step to API request
* application is hosted in docker container on ec2 instance
Make a GET request to `52.64.207.240/api/user/login` it will return a auth token

## APIs
### all the apis will be accessed using Authorization token only
* Make a POST request to `52.64.207.240/api/dinosaur/` to create a dinosaur by passing the Authorization token in headers `Bearer token`  
sample request  ```{
	"name": "first one",
	"era"  :"triassic",
	"diet": "herbivore"
}```
* Make a PATCH request to `52.64.207.240/api/dinosaur/` to update a dinosaur
sample request ``` {
	"name": "new",
	"era"  :"cretaceous",
	"diet":"herbivore",
	"_id":"5c5c0a93ea48c0bc5ffe5273"
}```
* Make a DELETE request to `52.64.207.240/api/dinosaur/` to delete a dinosaur by id 
sample request ```{
	"_id":"5c5c0a49ea48c0bc5ffe5272"
}```

* Make a GET request to `52.64.207.240/api/dinosaur/search?diet=herbivore&era=cretaceous` to search by era and id


* Make a GET request to `52.64.207.240/api/dinosaur/` to get all dinasaurs

