const request = require('supertest');
const jwt = require('jsonwebtoken');
const app = require('../app');
const Dinosaur = require('../api/models/dinosaur');

const agent = request(app);
let token;
let userid;
describe('Dinosour api -- Unit Tests', async () => {
  beforeAll(async () => {
    const { body: loginDetails } = await agent.get('/api/user/login');
    token = loginDetails.token;
    const decoded = jwt.verify(token, process.env.JWT_KEY);
    userid = decoded.userId;
  });

  it('to test- get all dinosaurs', async () => {
    try {
      const dinosaurList = await Dinosaur.find({ userId: userid });
      const dbRespose = {
        success: true,
        dinosaurList,
      };
      const { body: apiResponse } = await agent.get('/api/dinosaur/')
        .set({
          Authorization: `Bearer ${token}`,
        });

      expect(apiResponse).toEqual(dbRespose);
    } catch (error) {
      console.log(error);
    }
  });
});
