const express = require('express');

const app = express();
const bodyParser = require('body-parser');
const helmet = require('helmet');
require('./api/config/mongoose');
const morgan = require('morgan');

app.use(morgan('dev'));

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(helmet());

app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', '*');
  if (req.method === 'OPTIONS') {
    res.header('Access-Control-Allow-Methods', 'GET, PATCH, DELETE, POST');
    return res.status(200).json({});
  }
  next();
});

// routes
app.use('/api', require('./api/routes'));

app.use((req, res, next) => {
  const error = new Error('Invalid request endpoint');
  error.status = 404;
  next(error);
});
app.use((error, req, res, next) => {
  res.status(error.status || 500).json({
    error: {
      message: error.message,
    },
  });
});


module.exports = app;
